resource "helmfile_release_set" "kube-prometheus-stack" {
    content = file("./helmfile.yaml")
    working_directory = "./"
    kubeconfig        = pathexpand("./kubeconfig.yaml")
    environment       = "prod"
}




# NAMESPACE CREATION

resource "kubernetes_namespace" "monitoring" {
  metadata {
    name = "monitoring"
  }
}


#resource "helmfile_release_set" "telegraf" {
#    content = file("./helmfile.yaml")
#    working_directory = "./"
#    kubeconfig        = pathexpand("./kubeconfig.yaml")
#    environment       = "prod"
#}


# TELEGRAF CONFIG MAP

#data  "kubernetes_config_map" "telegraf-ds" {
#  metadata {
#    name      = "telegraf-ds"
#    namespace = kubernetes_namespace.monitoring.metadata[0].name
#  }
#}

resource  "kubernetes_config_map" "telegraf-ds" {
  metadata {
    name      = "telegraf-ds"
    namespace = kubernetes_namespace.monitoring.metadata[0].name
    #resource_version = "3756030"
    labels = {
      "app.kubernetes.io/managed-by" = "Helm"
      "app.kubernetes.io/name" =  "telegraf-ds"
      "app.kubernetes.io/instance" = "telegraf-ds"
#      "helm.sh/chart" =  "telegraf-ds-1.0.22"
    }
    annotations = {
      "meta.helm.sh/release-name" = "telegraf-ds"
      "meta.helm.sh/release-namespace" = "monitoring"
    }
  }

  data = {
    "telegraf.conf"        = file("${path.root}/telegraf-ds.toml")
  }
}


# VMWARE DASHBOARDS

resource "kubernetes_config_map" "grafana-dashboards-vmware-vsphere-overview" {
  metadata {
    name      = "grafana-dashboards-vmware-vsphere-overview"
    namespace = kubernetes_namespace.monitoring.metadata[0].name 

    labels = {
      grafana_dashboard = 1
    }

    annotations = {
      k8s-sidecar-target-directory = "/tmp/dashboards/VMware"
    }
  }
  data = {
    "vmware-vsphere-overview_rev38.json"        = file("${path.root}/grafana-dashboards/vmware-vsphere-overview_rev38.json")
  }
}


resource "kubernetes_config_map" "grafana-dashboards-vcenter-vm-details" {
  metadata {
    name      = "grafana-dashboards-vcenter-vm-details"
    namespace = kubernetes_namespace.monitoring.metadata[0].name

    labels = {
      grafana_dashboard = 1
    }

    annotations = {
      k8s-sidecar-target-directory = "/tmp/dashboards/VMware"
    }
  }

  data = {
    "vcenter-virtualmachine-details_rev1.json"        = file("${path.root}/grafana-dashboards/vcenter-virtualmachine-details_rev1.json")
  }
}


resource "kubernetes_config_map" "grafana-dashboards-vsphere-host-details" {
  metadata {
    name      = "grafana-dashboards-vsphere-host-details"
    namespace = kubernetes_namespace.monitoring.metadata[0].name

    labels = {
      grafana_dashboard = 1
    }

    annotations = {
      k8s-sidecar-target-directory = "/tmp/dashboards/VMware"
    }
  }

  data = {
    "vsphere-host-details_rev3.json"        = file("${path.root}/grafana-dashboards/vsphere-host-details_rev3.json")
  }
}


resource "kubernetes_config_map" "grafana-dashboards-vmware-vsphere-vms" {
  metadata {
    name      = "grafana-dashboards-vmware-vsphere-vms"
    namespace = kubernetes_namespace.monitoring.metadata[0].name

    labels = {
      grafana_dashboard = 1
    }

    annotations = {
      k8s-sidecar-target-directory = "/tmp/dashboards/VMware"
    }
  }

  data = {
    "vmware-vsphere-vms_rev14.json"        = file("${path.root}/grafana-dashboards/vmware-vsphere-vms_rev14.json")
  }
}





# AWS DASHBOARDS

resource "kubernetes_config_map" "grafana-dashboards-aws-cloudwatch-logs" {
  metadata {
    name      = "grafana-dashboards-aws-cloudwatch-logs"
    namespace = kubernetes_namespace.monitoring.metadata[0].name

    labels = {
      grafana_dashboard = 1
    }

    annotations = {
      k8s-sidecar-target-directory = "/tmp/dashboards/AWS"
    }
  }

  data = {
    "cloudwatch-logs_rev1.json"        = file("${path.root}/grafana-dashboards/cloudwatch-logs_rev1.json")
  }
}


resource "kubernetes_config_map" "grafana-dashboards-aws-lambda" {
  metadata {
    name      = "grafana-dashboards-aws-lambda"
    namespace = kubernetes_namespace.monitoring.metadata[0].name

    labels = {
      grafana_dashboard = 1
    }

    annotations = {
      k8s-sidecar-target-directory = "/tmp/dashboards/AWS"
    }
  }

  data = {
    "aws-lambda_rev12.json"        = file("${path.root}/grafana-dashboards/aws-lambda_rev12.json")
  }
}



resource "kubernetes_config_map" "grafana-dashboards-aws-billing" {
  metadata {
    name      = "grafana-dashboards-aws-billing"
    namespace = kubernetes_namespace.monitoring.metadata[0].name

    labels = {
      grafana_dashboard = 1
    }

    annotations = {
      k8s-sidecar-target-directory = "/tmp/dashboards/AWS"
    }
  }

  data = {
    "aws-billing.json"        = file("${path.root}/grafana-dashboards/aws-billing.json")
  }
}


resource "kubernetes_config_map" "grafana-dashboards-aws-api-gateway" {
  metadata {
    name      = "grafana-dashboards-api-gateway"
    namespace = kubernetes_namespace.monitoring.metadata[0].name

    labels = {
      grafana_dashboard = 1
    }

    annotations = {
      k8s-sidecar-target-directory = "/tmp/dashboards/AWS"
    }
  }

  data = {
    "aws-api-gateway.json"        = file("${path.root}/grafana-dashboards/aws-api-gateway.json")
  }
}















# POSTGRESQL DASHBOARD (FOR GITLAB)

resource "kubernetes_config_map" "grafana-dashboards-postgresql-database-overview" {
  metadata {
    name      = "grafana-dashboards-postgresql-database-overview"
    namespace = kubernetes_namespace.monitoring.metadata[0].name

    labels = {
      grafana_dashboard = 1
    }

    annotations = {
      k8s-sidecar-target-directory = "/tmp/dashboards/Postgresql"
    }
  }

  data = {
    "postgresql-gitlab-database_rev7.json"        = file("${path.root}/grafana-dashboards/postgresql-gitlab-database_rev7.json")
  }
}


resource "kubernetes_config_map" "grafana-dashboards-postgresql-overview" {
  metadata {
    name      = "grafana-dashboards-postgresql-overview"
    namespace = kubernetes_namespace.monitoring.metadata[0].name

    labels = {
      grafana_dashboard = 1
    }

    annotations = {
      k8s-sidecar-target-directory = "/tmp/dashboards/Postgresql"
    }
  }

  data = {
    "postgres-overview_rev2.json"        = file("${path.root}/grafana-dashboards/postgres-overview_rev2.json")
  }
}











# HASHICORP NOMAD DASHBOARD


resource "kubernetes_config_map" "grafana-dashboards-nomad-cluster" {
  metadata {
    name      = "grafana-dashboards-nomad-cluster"
    namespace = kubernetes_namespace.monitoring.metadata[0].name

    labels = {
      grafana_dashboard = 1
    }

    annotations = {
      k8s-sidecar-target-directory = "/tmp/dashboards/Hashicorp"
    }
  }

  data = {
    "nomad-cluster_rev1.json"        = file("${path.root}/grafana-dashboards/nomad-cluster_rev1.json")
  }
}



# JIRA DASHBOARDS

resource "kubernetes_config_map" "grafana-dashboards-jira" {
  metadata {
    name      = "grafana-dashboards-jira"
    namespace = kubernetes_namespace.monitoring.metadata[0].name

    labels = {
      grafana_dashboard = 1
    }

    annotations = {
      k8s-sidecar-target-directory = "/tmp/dashboards/Jira"
    }
  }

  data = {
    "jira_rev1.json"        = file("${path.root}/grafana-dashboards/jira_rev1.json")
  }
}


resource "kubernetes_config_map" "grafana-dashboards-jira-server" {
  metadata {
    name      = "grafana-dashboards-jira-server"
    namespace = kubernetes_namespace.monitoring.metadata[0].name

    labels = {
      grafana_dashboard = 1
    }

    annotations = {
      k8s-sidecar-target-directory = "/tmp/dashboards/Jira"
    }
  }

  data = {
    "jira-server_rev1.json"        = file("${path.root}/grafana-dashboards/jira-server_rev1.json")
  }
}




# MYSQL DASHBOARDS


resource "kubernetes_config_map" "grafana-dashboards-mysql-overview" {
  metadata {
    name      = "grafana-dashboards-mysql-overview"
    namespace = kubernetes_namespace.monitoring.metadata[0].name

    labels = {
      grafana_dashboard = 1
    }

    annotations = {
      k8s-sidecar-target-directory = "/tmp/dashboards/MySQL"
    }
  }

  data = {
    "mysql-overview_rev5.json"        = file("${path.root}/grafana-dashboards/mysql-overview_rev5.json")
  }
}



resource "kubernetes_config_map" "grafana-dashboards-mysql" {
  metadata {
    name      = "grafana-dashboards-mysql"
    namespace = kubernetes_namespace.monitoring.metadata[0].name

    labels = {
      grafana_dashboard = 1
    }

    annotations = {
      k8s-sidecar-target-directory = "/tmp/dashboards/MySQL"
    }
  }

  data = {
    "mysql_rev1.json"        = file("${path.root}/grafana-dashboards/mysql_rev1.json")
  }
}


# HASHICORP VAULT DASHBOARDS


resource "kubernetes_config_map" "grafana-dashboards-hashicorp-vault" {
  metadata {
    name      = "grafana-dashboards-hashicorp-vault"
    namespace = kubernetes_namespace.monitoring.metadata[0].name

    labels = {
      grafana_dashboard = 1
    }

    annotations = {
      k8s-sidecar-target-directory = "/tmp/dashboards/Hashicorp"
    }
  }

  data = {
    "hashicorp-vault_rev2.json"        = file("${path.root}/grafana-dashboards/hashicorp-vault_rev2.json")
  }
}


# HASHICORP CONSUL DASHBOARDS


resource "kubernetes_config_map" "grafana-dashboards-hashicorp-consul" {
  metadata {
    name      = "grafana-dashboards-hashicorp-consul"
    namespace = kubernetes_namespace.monitoring.metadata[0].name

    labels = {
      grafana_dashboard = 1
    }

    annotations = {
      k8s-sidecar-target-directory = "/tmp/dashboards/Hashicorp"
    }
  }

  data = {
    "hashicorp-consul_rev1.json"        = file("${path.root}/grafana-dashboards/hashicorp-consul_rev1.json")
  }
}


resource "kubernetes_config_map" "grafana-dashboards-hashicorp-consul-monitoring" {
  metadata {
    name      = "grafana-dashboards-hashicorp-consul-monitoring"
    namespace = kubernetes_namespace.monitoring.metadata[0].name

    labels = {
      grafana_dashboard = 1
    }

    annotations = {
      k8s-sidecar-target-directory = "/tmp/dashboards/Hashicorp"
    }
  }

  data = {
    "hashicorp-consul-monitoring_rev1.json"        = file("${path.root}/grafana-dashboards/hashicorp-consul-monitoring_rev1.json")
  }
}


# GETH

resource "kubernetes_config_map" "grafana-dashboards-blockchain-geth-server" {
  metadata {
    name      = "grafana-dashboards-blockchain-geth-server"
    namespace = kubernetes_namespace.monitoring.metadata[0].name

    labels = {
      grafana_dashboard = 1
    }

    annotations = {
      k8s-sidecar-target-directory = "/tmp/dashboards/Blockchain"
    }
  }

  data = {
    "geth-server_rev3.json"        = file("${path.root}/grafana-dashboards/geth-server_rev3.json")
  }
}

