terraform {
  required_providers {
    helmfile = {
      source = "mumoshu/helmfile"
      version = "0.14.0"
    }
    docker = {
       source = "kreuzwerker/docker"
    }
  }
}


provider "docker" {
  host = "unix:///var/run/docker.sock"
}

provider "kubernetes" {
  config_path    = "./kubeconfig.yaml"
  config_context = "default"  
}

